**Semester Project: Tetris in C++ (Year 2024)**

-Implementation Description:::::::::
♦Game Loop: The main game logic is implemented in the gameLoop() function.
♦Shape Control: Using the inputOfPlayer() function, the player can control the falling shapes.
♦Collision Detection: The isCollide() function checks if the falling shape collides with other elements on the field.
♦Shape Rotation: The rotateShape() function implements the rotation of the current falling shape.
♦Displaying the Board: Functions paintingPlayingArea() and paintingShapes() are responsible for displaying the current state of the game board and falling shapes.

-Functionality and Application Controls:::::::::::
♦ Controls: Players can control the falling shapes using the keys: a (left), d (right), s (down), r (rotate).
♦ Interface: Console-based display of the game board using characters "#", "X", and "."
♦ Starting the Game: Upon launching the game, players are prompted to start by pressing 1.

-Threads:::::::::
In this version of the program, multithreading was not implemented, so there's no comparison with a single-threaded version.


Test inputs: 
1) a + enter
2) d + enter 
3) s + enter
4) r + enter
