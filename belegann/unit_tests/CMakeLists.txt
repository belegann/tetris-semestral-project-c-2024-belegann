#cmake_minimum_required(VERSION 3.26)
#project(belegann)
#
#include_directories(${CMAKE_SOURCE_DIR}/tests/lib/Catch2/src)
#
#add_executable(unit_tests main.test.cpp)
#
#target_link_libraries(unit_tests Catch2::Catch2)