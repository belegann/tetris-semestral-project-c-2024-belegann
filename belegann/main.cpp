#include <iostream>
#include <vector>
#include <conio.h>
#include <chrono>
#include <thread>
#include <random>

//methods:
void title();
void gameLoop();
void makingField();
void paintingPlayingArea();
void paintingShapes();
void inputOfPlayer();
void moveFallingShape(int i, int y);
void rotateShape();
void cls();
bool isCollide(int i, int y);

//a struct that generates random integers within a specified range using
struct RandomGenerator{
    RandomGenerator(int min, int max)
    : mUniformDistribution(min, max) {}

    int operator()() {
        return mUniformDistribution(mEngine);
    }
    std::default_random_engine mEngine{ std::random_device()()};
    std::uniform_int_distribution<int> mUniformDistribution;

};
//variables:
bool isGameOver = false;

//coordinates:
int x = 4; //representing the horizontal position of the block
int y = 0; //representing the vertical position of the block

//Vector of vector for field (22 vectors - each has 22 zeros)
std::vector<std::vector<int>> gameField(22, std::vector<int>(22, 0));

//Same vector as field vector but is used to store the state of the game "substrate", which is a static part of the playing field
std::vector<std::vector<int>> gameFieldForStaticShapes(22, std::vector<int>(22, 0));

//Empty pattern square for future shapes
std::vector<std::vector<int>> patternSquareForShapes =
        {
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0}
        };

//Creating vector in vector in vector for shapes in our game, we have 7 shapes
std::vector<std::vector<std::vector<int>>> fallingShapes =
        {
                {
                        {1, 1, 1, 1},
                        {0, 0, 0, 0},
                        {0, 0, 0, 0},
                        {0, 0, 0, 0}
                },
                {
                        {0, 0, 0, 0},
                        {0, 1, 1, 0},
                        {0, 1, 1, 0},
                        {0, 0, 0, 0}
                },
                {
                        {0, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 1, 1, 0},
                        {0, 0, 0, 0}
                },
                {
                        {0, 0, 1, 0},
                        {0, 1, 1, 0},
                        {0, 1, 0, 0},
                        {0, 0, 0, 0}
                },
                {
                        {0, 1, 0, 0},
                        {0, 1, 1, 0},
                        {0, 0, 1, 0},
                        {0, 0, 0, 0}
                },
                {
                        {0, 0, 0, 0},
                        {0, 1, 0, 0},
                        {1, 1, 1, 0},
                        {0, 0, 0, 0}
                },
                {
                        {0, 0, 0, 0},
                        {1, 0, 0, 0},
                        {1, 1, 1, 0},
                        {0, 0, 0, 0}
                },

        };

RandomGenerator shuffleShapes{0, 6}; //an instance of the Random struct that generates random numbers between 0 and 6

int main()
{
    title();
    int selectedButton = 0;

    do {
        std::cin >> selectedButton;

        if (selectedButton == 1) {
            gameLoop();
            break;
        } else {
            std::cout << "You should press 1 to start game!\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    } while (true);
    return 0;
}

/**
 * @brief Main game loop that manages the flow of the game
 */
void gameLoop() {
    makingField();
    using namespace std::chrono;
    milliseconds ms(1000); //1 second
    while (!isGameOver) {
        auto start = high_resolution_clock::now();

        if (kbhit()) {
            inputOfPlayer();
        }

        auto end = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(end - start);

        if (duration < ms) {
            std::this_thread::sleep_for(ms - duration);

        }
        if (!isCollide(x, y+1)) {moveFallingShape(x, y+1);}
        else {
            if (y == 1) {
                isGameOver = true;
                cls();
            }
            gameFieldForStaticShapes = gameField;
            paintingShapes();
            paintingPlayingArea();
        }

    }
}


/**
 * @brief Accepts user input to move or rotate falling shape
 */
void inputOfPlayer() {
    char input = getch();
    switch (input) {
        case 'a':
            if (!isCollide(x-1, y)) {moveFallingShape(x-1, y);} break;
        case 'd':
            if (!isCollide(x+1, y)) {moveFallingShape(x+1, y);} break;
        case 's':
            if (!isCollide(x, y+1)) {moveFallingShape(x, y+1);} break;
        case 'r':
            rotateShape();
    }
}

/**
 * Checks if the falling shape collides with any static shapes on the field
 * @param x x-coordinate of the shape
 * @param y y-coordinate of the shape
 * @return True if
 */
bool isCollide(int x, int y) {
    for (size_t i = 0; i < 4; i++) {
        for (size_t j = 0; j < 4; j++) {
            if(patternSquareForShapes[i][j] && gameFieldForStaticShapes[y + i][x + j] != 0) {
                return true;
            }
        }
    }
    return false;
}

/**
 * @brief Rotates the current falling shape
 */
void rotateShape() {
    std::vector<std::vector<int>> temporaryField = patternSquareForShapes;

    //Rotate clockwise
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            patternSquareForShapes[j][3-i] = temporaryField[i][j];
        }
    }

    //isCollide (later)
    if(isCollide(x, y)) {
        patternSquareForShapes = temporaryField;
    }

    for (size_t i = 0; i < 4; i++)
    {
        for (size_t j = 0; j < 4; j++)
        {
            gameField[y + i][x + j] -= temporaryField[i][j];
            gameField[y + i][x + j] += patternSquareForShapes[i][j];
        }
    }

    paintingPlayingArea();
}

/**
 * @brief Moves the falling shape to a new position
 * @param newX New x-coordinate for the shape
 * @param newY New y-coordinate for the shape
 */
void moveFallingShape(int newX, int newY) {

    for (size_t i = 0; i < 4; i++) {
        for (size_t j = 0; j < 4; j++) {
            gameField[y + i][x + j] = gameField[y + i][x + j] - patternSquareForShapes[i][j];
        }
    }

    //updating coordinates
    x = newX;
    y = newY;

    for (size_t i = 0; i < 4; i++) {
        for (size_t j = 0; j < 4; j++) {
            gameField[y + i][x + j] = gameField[y + i][x + j] + patternSquareForShapes[i][j];
        }
    }

    paintingPlayingArea();
}

/**
 * @brief Initialize the gameField, setting up initial boundaries and properties
 */
void makingField() {
    //Field is 20-width, 11-height
    for (size_t i = 0; i < 21; i++) {
        for (size_t j = 0; j < 21; j++) {
            //Conditions for making border of field (right side, left side, the floor)
            //Border is 9 symbol
            if ((j == 0) || (i == 20) || (j == 20)  || (i == 0)) {
                gameField[i][j] = gameFieldForStaticShapes[i][j] = 9;
            }
            else {
                gameField[i][j] = gameFieldForStaticShapes[i][j] = 0;
            }
        }
    }
    paintingShapes();
    paintingPlayingArea();
}

/**
 * @brief Determines the shapes to paint and manages the painting process
 */
void paintingShapes() {
    //Coordinates for falling shape
    x = 4; //starting not from the (0, 0) but (4, 0)
    y = 0;


    int numberOfShape = shuffleShapes(); //choosing shape from our list randomly

    //iterating the square block for shapes
    for (size_t i = 0; i < 4; i++) {
        for (size_t j = 0; j < 4; j++) {
            patternSquareForShapes[i][j] = 0;
            patternSquareForShapes[i][j] = fallingShapes[numberOfShape][i][j];
        }
    }

    //painting shapes on field
    for (size_t i = 0; i < 4; i++) {
        for (size_t j = 0; j < 4; j++) {

            gameField[i][j + 4] = gameFieldForStaticShapes[i][j+4] + patternSquareForShapes[i][j];

        }
    }
}


/**
 * @brief Painting playing area/field 22*22
 */
void paintingPlayingArea() {
    cls();
    //Iterating vector of vectors (game field) (22 vectors - each has 22 dots)
    for (size_t rows = 0; rows < 21; rows++) {
        for (size_t colums = 0; colums < 21; colums++) {
            if (gameField[rows][colums] == 0) { //empty space
                std::cout << ".";
            }
            else if (gameField[rows][colums] == 9) { //border
                std::cout << "#";
            }
            else {
                std::cout << "X"; //shape
            }
        }
        std::cout << "\n";
    }
    std::cout << "=============================================================================================\n";
    std::cout << "PRESS - a + ENTER - TO MOVE LEFT\n";
    std::cout << "PRESS - d + ENTER - TO MOVE RIGHT\n";
    std::cout << "PRESS - s + ENTER - TO MOVE DOWN\n";
    std::cout << "PRESS - r + ENTER - TO ROTATE\n";
    if (isGameOver)
    {
        cls();
        std::cout << "THE GAME IS OVER!!!!!!!!!!!!!!!!\n";
        std::cout << "THE GAME IS OVER!!!!!!!!!!!!!!!!\n";
        std::cout << "THE GAME IS OVER!!!!!!!!!!!!!!!!\n";
        std::cout << "THE GAME IS OVER!!!!!!!!!!!!!!!!\n";
        std::cout << "THE GAME IS OVER!!!!!!!!!!!!!!!!\n";
    }

}

/**
 * @brief Clears the console screen
 */
void cls()
{
    std::cout << "\x1b[2J";
    #ifdef _WIN32
        system("cls");
    #else
        system("clear");
    #endif
}

/**
 * @brief Display the title of the tetris game and promt user to start
 */
void title() {
    using namespace std;
    cout << "***************************************************************************************\n";
    cout << "SEMESTRAL WORK TETRIS!\n";
    cout << "Press 1 to start!\n";
    cout << "**************************************************************************************\n";
    cout << ">>";
    cout << " ";
}

